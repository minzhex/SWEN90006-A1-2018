package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }
  //t1
  @Test(expected = InvalidInstructionException.class)
  public void t1() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R-1 1");
    list.add("RET R-1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }


  //t2
  @Test(expected = InvalidInstructionException.class)
  public void t2() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R32 1");
    list.add("ADD R32 R32 R32");
    list.add("RET R32");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  /**
   * failed test
  //t3
  @Test(expected = InvalidInstructionException.class)
  public void t3() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("RET R1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }
  */

  //t4
  @Test(expected = InvalidInstructionException.class)
  public void t4() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 -65536");
    list.add("RET R1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //t5
  @Test(expected = InvalidInstructionException.class)
  public void t5() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("JMP 65536");
    for(int i=0; i<66666;i++){
      list.add("MOV R1 1");
    }
    list.add("RET R1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //t6
  @Test(expected = NoReturnValueException.class)
  public void t6() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //t7
  @Test(expected = NoReturnValueException.class)
  public void t7() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("JMP 2");
    list.add("RET R1");
    list.add("MOV R1 1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

/**
 * failed test
  //t8
  @Test(expected = InvalidInstructionException.class)
  public void t8() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("ADD R1 R1 R1");
    list.add("SUB R1 R1 R2");
    list.add("DIV R1 R1 R2");
    list.add("RET R1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }
  */

  //t9
  @Test public void t9() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("ADD R1 R1 R1");
    list.add("SUB R1 R1 R2");
    list.add("DIV R1 R1 R1");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }
  //t10-
  @Test public void t10() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 -1");
    list.add("MOV R2 0");
    list.add("ADD R1 R1 R1");
    list.add("SUB R1 R1 R2");
    list.add("DIV R1 R2 R1");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 0;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //td1
  @Test public void td1() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 -65535");
    list.add("MOV R2 65535");
    list.add("ADD R3 R1 R1");
    list.add("SUB R4 R1 R2");
    list.add("DIV R1 R1 R2");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = -1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }
  //td2
  @Test public void td2() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 65535");
    list.add("MOV R2 -65535");
    list.add("ADD R3 R1 R1");
    list.add("SUB R4 R1 R2");
    list.add("DIV R1 R1 R2");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = -1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //t11
  @Test public void t11() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R0 -65535");
    list.add("MOV R1 65535");
    list.add("SUB R0 R0 R1");
    list.add("ADD R1 R1 R1");
    list.add("RET R0");
    Machine m = new Machine();
    final int expected = -65535-65535;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //t12
  @Test public void t12() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R31 65535");
    list.add("MOV R1 -65535");
    list.add("SUB R31 R31 R1");
    list.add("ADD R1 R1 R1");
    list.add("RET R31");
    Machine m = new Machine();
    final int expected = 65535+65535;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //t13
  @Test public void t13() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 -65535");
    list.add("MOV R2 65535");
    list.add("ADD R3 R1 R2");
    list.add("JMP 2");
    list.add("MOV R3 1");
    list.add("RET R3");
    Machine m = new Machine();
    final int expected = 0;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //t14
  @Test(expected = NoReturnValueException.class)
  public void t14() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("JMP -2");
    list.add("MOV R2 0");
    list.add("ADD R1 R1 R1");
    list.add("RET R1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //t15
  @Test(expected = NoReturnValueException.class)
  public void t15() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("ADD R1 R1 R1");
    list.add("JMP 2");
    list.add("RET R1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //t16
  @Test public void t16() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 65535");
    list.add("MOV R2 -65535");
    list.add("ADD R1 R1 R2");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 0;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //t17
  @Test public void t17() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("MUL R2 R1 R2");
    list.add("JZ R2 2");
    list.add("RET R1");
    list.add("RET R2");
    Machine m = new Machine();
    final int expected = 0;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }


  //t18
  @Test public void t18() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 -32768");
    list.add("MOV R2 32768");
    list.add("MOV R3 1");
    list.add("MUL R1 R1 R2");
    list.add("JZ R3 2");
    list.add("RET R1");
    list.add("RET R2");
    Machine m = new Machine();
    final int expected = -32768*32768;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //t19
  @Test public void t19() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 32768");
    list.add("MOV R2 -32768");
    list.add("MOV R3 -1");
    list.add("MUL R1 R1 R2");
    list.add("JZ R3 2");
    list.add("RET R1");
    list.add("RET R2");
    Machine m = new Machine();
    final int expected = -32768*32768;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //t20
  @Test public void t20() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 65535");
    list.add("MOV R2 -32768");
    list.add("MOV R31 32768");
    list.add("MUL R2 R1 R2");
    list.add("SUB R2 R2 R31");
    list.add("RET R2");
    Machine m = new Machine();
    final int expected = -2147483648;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //t21
  @Test public void t21() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 65535");
    list.add("MOV R2 32768");
    list.add("MOV R31 -32767");
    list.add("MUL R2 R1 R2");
    list.add("SUB R2 R2 R31");
    list.add("RET R2");
    Machine m = new Machine();
    final int expected = 2147483647;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

/**
 * failed test
  //t22
  @Test(expected = InvalidInstructionException.class)
  public void t22() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 65535");
    list.add("MOV R2 -32768");
    list.add("MOV R31 32769");
    list.add("MUL R2 R1 R2");
    list.add("SUB R2 R2 R31");
    list.add("RET R2");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //t23
  @Test(expected = InvalidInstructionException.class)
  public void t23() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 65535");
    list.add("MOV R2 32768");
    list.add("MOV R31 -32768");
    list.add("MUL R2 R1 R2");
    list.add("SUB R2 R2 R31");
    list.add("RET R2");
    Machine m = new Machine();
    int actual = m.execute(list);
  }
  */

  //t24
  @Test public void t24() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 65535");
    list.add("STR R2 -65535 R1");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //t25
  @Test public void t25() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("STR R3 65535 R1");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }


  //t26
  @Test public void t26() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 -65535");
    list.add("MOV R3 -1");
    list.add("STR R2 65535 R1");
    list.add("LDR R3 R2 65535");
    list.add("RET R3");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }
  //t27
  @Test public void t27() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 65535");
    list.add("MOV R3 -1");
    list.add("STR R2 -65535 R1");
    list.add("LDR R3 R2 -65535");
    list.add("RET R3");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //t28
  @Test public void t28() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("MOV R3 -1");
    list.add("STR R2 65535 R1");
    list.add("LDR R3 R2 65535");
    list.add("RET R3");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }
  //t29
  @Test public void t29() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 65535");
    list.add("MOV R3 -1");
    list.add("SUB R2 R2 R3");
    list.add("STR R2 -1 R1");
    list.add("LDR R3 R2 -1");
    list.add("RET R3");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }
  //t30
  @Test public void t30() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 65535");
    list.add("MOV R3 -1");
    list.add("STR R2 -65535 R3");
    list.add("LDR R1 R3 0");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //t31
  @Test public void t31() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 65535");
    list.add("MOV R3 -1");
    list.add("STR R2 -65535 R3");
    list.add("LDR R1 R3 0");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }


  //t32
  @Test public void t32() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("MOV R3 -1");
    list.add("STR R3 0 R3");
    list.add("RET R3");
    Machine m = new Machine();
    final int expected = -1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //t33
  @Test public void t33() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("MOV R3 -1");
    list.add("STR R1 65535 R3");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //t34
  @Test public void t34() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }
  //t35
  @Test public void t35() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

}
