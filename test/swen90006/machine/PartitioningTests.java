
package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //ec1
  @Test(expected = InvalidInstructionException.class)
  public void ec1() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("ADD R1 R2 R3");
    list.add("RET R1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //ec2
  @Test(expected = InvalidInstructionException.class)
  public void ec2() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 -65536");
    list.add("MOV R2 -1");
    list.add("ADD R1 R1 R2");
    list.add("RET R1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //ec3
  @Test(expected = InvalidInstructionException.class)
  public void ec3() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 65535");
    list.add("ADD R1 R1 R1");
    list.add("RET R1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //ec4
  @Test(expected = InvalidInstructionException.class)
  public void ec4() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 65536");
    list.add("MOV R1 -65536");
    list.add("RET R1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //ec5
  @Test(expected = NoReturnValueException.class)
  public void ec5() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //ec6
  @Test(expected = NoReturnValueException.class)
  public void ec6() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("JMP 3");
    list.add("MOV R1 1");
    list.add("RET R1");
    list.add("MOV R1 1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //ec7
  @Test(expected = InvalidInstructionException.class)
  public void ec7() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R-1 1");
    list.add("RET R-1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //ec8
  @Test(expected = InvalidInstructionException.class)
  public void ec8() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R32 1");
    list.add("RET R32");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //ec9
  @Test(expected = InvalidInstructionException.class)
  public void ec9() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("ADD R1 R1 R1");
    list.add("SUB R1 R1 R2");
    list.add("DIV R1 R1 R2");
    list.add("RET R1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //ec10
  @Test public void ec10() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("ADD R1 R1 R1");
    list.add("SUB R1 R1 R2");
    list.add("DIV R1 R1 R1");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec11
  @Test public void ec11() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("ADD R1 R1 R1");
    list.add("SUB R1 R1 R2");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 2;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec12
  @Test public void ec12() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("JMP 2");
    list.add("MOV R2 0");
    list.add("ADD R1 R1 R1");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 2;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec13
  @Test(expected = NoReturnValueException.class)
  public void ec13() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("JMP -2");
    list.add("MOV R2 0");
    list.add("ADD R1 R1 R1");
    list.add("RET R1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //ec14
  @Test(expected = NoReturnValueException.class)
  public void ec14() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("JMP 4");
    list.add("MOV R2 0");
    list.add("ADD R1 R1 R1");
    list.add("RET R1");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //ec15
  @Test public void ec15() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("ADD R1 R1 R1");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 2;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec16
  @Test public void ec16() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("MUL R2 R1 R2");
    list.add("JZ R2 2");
    list.add("RET R1");
    list.add("RET R2");
    Machine m = new Machine();
    final int expected = 0;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec17
  @Test public void ec17() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("MUL R2 R1 R2");
    list.add("JZ R1 2");
    list.add("RET R1");
    list.add("RET R2");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec18
  @Test public void ec18() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("MUL R2 R1 R2");
    list.add("RET R2");
    Machine m = new Machine();
    final int expected = 0;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec19
  @Test public void ec19() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("STR R2 65535 R1");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec20
  @Test public void ec20() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("MOV R3 -1");
    list.add("STR R2 65535 R1");
    list.add("LDR R3 R2 65535");
    list.add("RET R3");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec21
  @Test public void ec21() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("MOV R3 -1");
    list.add("STR R2 -65535 R3");
    list.add("LDR R1 R3 -65535");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec22
  @Test public void ec22() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("MOV R3 -1");
    list.add("STR R2 65535 R1");
    list.add("LDR R3 R1 65535");
    list.add("RET R3");
    Machine m = new Machine();
    final int expected = -1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec23
  @Test public void ec23() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("MOV R3 -1");
    list.add("STR R2 65535 R2");
    list.add("LDR R1 R1 65535");
    list.add("LDR R1 R3 -65535");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec24
  @Test public void ec24() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("MOV R3 -1");
    list.add("STR R3 -65535 R3");
    list.add("LDR R1 R3 -65535");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec25
  @Test public void ec25() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("MOV R3 -1");
    list.add("STR R3 -65535 R3");
    list.add("LDR R1 R3 -65535");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec26
  @Test public void ec26() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("MOV R2 0");
    list.add("MOV R3 -1");
    list.add("STR R3 -65535 R3");
    list.add("STR R1 65535 R1");
    list.add("LDR R2 R3 -65535");
    list.add("LDR R2 R1 65535");
    list.add("RET R2");
    Machine m = new Machine();
    final int expected = 0;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec27
  @Test public void ec27() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

  //ec28
  @Test(expected = InvalidInstructionException.class)
  public void ec28() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R0 32768");
    list.add("MOV R1 2");
    list.add("MOV R3 -1");
    list.add("MUL R2 R0 R0");
    list.add("MUL R2 R2 R3");
    list.add("MUL R2 R2 R1");
    list.add("ADD R2 R2 R3");
    list.add("RET R2");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //ec29
  @Test(expected = InvalidInstructionException.class)
  public void ec29() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R0 32768");
    list.add("MOV R1 2");
    list.add("MUL R2 R0 R0");
    list.add("MUL R2 R2 R1");
    list.add("RET R2");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //ec30
  @Test(expected = InvalidInstructionException.class)
  public void ec30() throws Throwable
  {
    List<String> list = new ArrayList<String>();
    list.add("MOV R0 32768");
    list.add("MOV R1 2");
    list.add("MOV R3 -1");
    list.add("MUL R2 R0 R0");
    list.add("MUL R2 R2 R3");
    list.add("MUL R2 R2 R1");
    list.add("MUL R4 R2 R3");
    list.add("ADD R2 R2 R3");
    list.add("RET R2");
    Machine m = new Machine();
    int actual = m.execute(list);
  }

  //ec31
  @Test public void ec31() {
    List<String> list = new ArrayList<String>();
    list.add("MOV R1 1");
    list.add("");
    list.add("RET R1");
    Machine m = new Machine();
    final int expected = 1;
    int actual = m.execute(list);
    assertEquals(expected, actual);
  }

}

